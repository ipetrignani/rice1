#!/bin/sh

# colors
GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)
RED=$(tput setaf 1)
# user dirs
USER_DIRS='.local/share/xorg dev downloads'
# suckless suite
SUITE='dmenu dwm scroll slstatus st'
CONF_SUITE='dwm slstatus st'

set_alsa () {
  amixer set Capture cap
  amixer set Capture 0dB
  amixer set "Internal Mic" cap
  amixer set "Internal Mic Boost" 20dB
}

make_dirs () {
  for dir in $USER_DIRS; do
    mkdir -pv "$dir"
  done
}

cp_dot () {
  cp -v dotfiles/.gitconfig .
  cp -v dotfiles/.mkshrc .
  cp -v dotfiles/.vimrc .
  cp -v dotfiles/.xinitrc-dwm .xinitrc
}

cp_etc () {
  printf '%s' "$PASS" | su -c "
  printf '\\n'

  sed -i '0,/1/s//0/' /etc/default/bluetooth
  cp -v dotfiles/etc/modprobe.d/* /etc/modprobe.d/
  cp -v dotfiles/etc/sysctl.d/* /etc/sysctl.d/
  cp -v dotfiles/etc/udev/rules.d/* /etc/udev/rules.d/
  cp -rv dotfiles/etc/X11/xorg.conf.d/ /etc/X11/"
}

get_suite () {
  for software in $SUITE; do
    git clone https://git.suckless.org/"$software" .suckless/"$software"
  done
}

conf_suite () {
  for software in $CONF_SUITE; do
    cp -v dotfiles/"$software"/config.h .suckless/"$software"
  done
}

make_suite () {
  for software in $SUITE; do
    cd .suckless/"$software" || exit

    printf '%s' "$PASS" | su -c "
    printf '\\n'

    make -s -j4 install"

    make clean
    cd ../.. || exit
  done
}

get_font () {
  fonts_dir="/usr/share/fonts/truetype/firacode"

  printf '%s' "$PASS" | su -c "
  printf '\\n'

  mkdir -pv ${fonts_dir}"

  for type in Bold Light Medium Regular Retina; do
    file_path="FiraCode-${type}.ttf"
    file_url="https://github.com/tonsky/FiraCode/blob/master/distr/ttf/FiraCode-${type}.ttf?raw=true"
    wget -O "${file_path}" "${file_url}"
  done

  printf '%s' "$PASS" | su -c "
  printf '\\n'

  mv -v FiraCode-* ${fonts_dir}"

  fc-cache -fv ${fonts_dir}
}

get_firefox () {
  printf '%s' "$PASS" | su -c "
  printf '\\n'

  wget -O firefox.tar.bz2 'https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en-US'
  tar fvx firefox.tar.bz2 --directory /opt
  rm -v firefox.tar.bz2

  update-alternatives --install /usr/bin/x-www-browser x-www-browser /opt/firefox/firefox 200
  update-alternatives --set x-www-browser /opt/firefox/firefox
  chown -R '${USER}':'${USER}' /opt/firefox*

  cp -v dotfiles/bin/firefox /usr/local/bin/firefox
  chmod +x /usr/local/bin/firefox"
}

clear
printf '%s' "${GREEN}"
printf " _           _        _ _       _\\n"
printf "(_)_ __  ___| |_ __ _| | |  ___| |__\\n"
printf "| | '_ \\/ __| __/ _\` | | | / __| '_ \\ \\n"
printf "| | | | \\__ \\ || (_| | | |_\\__ \\ | | |\\n"
printf "|_|_| |_|___/\\__\\__,_|_|_(_)___/_| |_|\\n"
printf "\\n"
printf '%s' " -> password: ${NORMAL}"
read -r PASS

printf '%s\n' " ${GREEN}-> set alsa levels...${NORMAL}"
sleep 1 && set_alsa

printf '%s\n' " ${GREEN}-> make extra dirs...${NORMAL}"
sleep 1 && make_dirs

printf '%s\n' " ${GREEN}-> copy dotfiles...${NORMAL}"
sleep 1 && cp_dot

printf '%s\n' " ${GREEN}-> copy /etc...${NORMAL}"
sleep 1 && cp_etc

printf '%s\n' " ${GREEN}-> get suckless suite...${NORMAL}"
sleep 1 && get_suite

printf '%s\n' " ${GREEN}-> conf suckless suite...${NORMAL}"
sleep 1 && conf_suite

printf '%s\n' " ${GREEN}-> make suckless suite...${NORMAL}"
sleep 1 && make_suite

printf '%s\n' " ${GREEN}-> get font...${NORMAL}"
sleep 1 && get_font

printf '%s\n' " ${GREEN}-> get latest firefox...${NORMAL}"
sleep 1 && get_firefox

printf '%s\n' " ${RED}-> self-destructing..."
sleep 1 && printf '%s\n' "3"
sleep 1 && printf '%s\n' "2"
sleep 1 && printf '%s\n' "1${NORMAL}"
sleep 1 && rm -rf dotfiles
