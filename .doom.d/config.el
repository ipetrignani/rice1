;; Load EXWM.
(require 'exwm)

;; Load EXWM default configuration.
(require 'exwm-config)
(exwm-config-default)

;; Set the initial number of workspaces (they can also be created later).
(setq exwm-workspace-number 10)

;; Set keybindings.
(exwm-input-set-key
 (kbd "<XF86AudioMute>")
 (lambda ()
   (interactive)
   (start-process-shell-command "amixer" nil "amixer set Master toggle")))

(exwm-input-set-key
 (kbd "<XF86AudioLowerVolume>")
 (lambda ()
   (interactive)
   (start-process-shell-command "amixer" nil "amixer set Master 2%-")))

(exwm-input-set-key
 (kbd "<XF86AudioRaiseVolume>")
 (lambda ()
   (interactive)
   (start-process-shell-command "amixer" nil "amixer set Master 2%+")))

(exwm-input-set-key
 (kbd "<XF86MonBrightnessDown>")
 (lambda ()
   (interactive)
   (start-process-shell-command "brightnessctl" nil "brightnessctl set 2%-")))

(exwm-input-set-key
 (kbd "<XF86MonBrightnessUp>")
 (lambda ()
   (interactive)
   (start-process-shell-command "brightnessctl" nil "brightnessctl set +2%")))

;; Load symon.
(require 'symon)

;; Set monitors.
(setq symon-monitors
      '(symon-linux-memory-monitor
        symon-linux-cpu-monitor
        symon-linux-network-rx-monitor
        symon-linux-network-tx-monitor
        symon-linux-battery-monitor
        symon-current-time-monitor))

;; Set refresh rate.
(setq symon-refresh-rate 1)

;; Turn on symon-mode.
(symon-mode)
