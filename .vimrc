set nocompatible " enable all features (no vi compatible)

" Colors {{{
syntax on " syntax highlighting
set background=dark " terminal
" }}}
" Folding {{{
set foldenable " enable folding
set foldlevelstart=10 " open most folds by default
set foldnestmax=10 " number of nested folds max
set foldmethod=indent " fold based on indent level
" }}}
" Misc {{{
set history=1000
" }}}
" Tabulation {{{
set expandtab " tabs are spaces
set tabstop=4 " number of visual spaces per TAB
set shiftwidth=4 " number of (auto)indent characters
set softtabstop=4 " number of spaces in TAB when editing
" }}}
" UI Config {{{
filetype plugin indent on " auto-detect filetype specific indent files
set backspace=indent,eol,start " powerful backspaces
set encoding=utf-8 " encoding
set guicursor=n-v-c:block,o:block,i:block,r:block,sm:block " all-block cursor
set lazyredraw " redraw only when we need to
set laststatus=2 " always show status bar
set ruler " show file stats
set textwidth=0 " don't wrap words
set showmode " show mode on status bar
set showcmd " show partial commands in bottom bar
set showmatch " show matching braces { [ ( ) ] }
set wildmenu " visual auto-complete for command menu
set wildchar=<Tab> " expand the command line using tab
set relativenumber " relative line numbers
set colorcolumn=80,120 " visual limit
" }}}
" Searching {{{
set incsearch " jump to the matches while typing
set hlsearch " highlight matches
" }}}

" vim: foldmethod=marker foldlevel=0
